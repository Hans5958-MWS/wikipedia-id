echo "This is for reading only! Do not run directly!"
exit 0

# Fresh start? Get Pywikibot on the pwb folder and run these below.
pwb generate_family_file
pwb generate_user_files

# Login to user account
pwb login

# Commands
py -m pwb_scripts.citation_fix -random -ns:0 -log

pwb info_getter -page:"Smartfren Telecom"
py -m pwb_scripts.citation_fix -page:"Smartfren Telecom"

py -m pwb_scripts.citation_fix -search:"Kompas Cyber Media" -ns:0 -log
py -m pwb_scripts.citation_fix -page:"Badan Pengkajian dan Penerapan Teknologi"
py -m pwb_scripts.citation_fix -file:citeweb.txt -ns:0 -log 
py -m pwb_scripts.citation_fix -file:jawapost.txt -ns:0 -summary:"Perbarui referensi situs berita Indonesia (perbaikan pranala [[Jawa Pos]])" -start:"Aby Nursetyanto" -log 
py -m pwb_scripts.citation_fix -search:"suaramerdeka.com" -ns:0 -log -summary:"Perbarui referensi situs berita Indonesia (perbaikan Suara Merdeka)"
py -m pwb_scripts.citation_fix -search:"PT VIVA MEDIA BARU" -ns:0 -log 