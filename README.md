# MWS - Wikipedia bahasa Indonesia

This is the collection of scripts I use for editing in [id.wikipedia.org](https://id.wikipedia.org).

## Contents

| Name | Type | Description | General usage? |
| - | - | - | - |
| citation_fix | Pywikibot script | Memperbarui referensi dari situs berita Indonesia

## License

MIT