#!/usr/bin/python3
"""
Memperbarui referensi dari situs berita Indonesia.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always		   The bot won't ask for confirmation when putting a page

-summary:		 Set the action summary message for the edit.

Every script has its own section with the script name as header.

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
from pwb_scripts.cf_modules.getters._modules import get_val_safe, process_name
from pwb_scripts.cf_modules.get_company import get_citation_fixer
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	AutomaticTWSummaryBot,
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
import mwparserfromhell
import pywikibot

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': '🤖 Perbarui referensi situs berita Indonesia (perbaikan author banyak dengan pemisah koma)',
	'minor': True,
}

class MultiAuthorFixBot(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
	AutomaticTWSummaryBot,
):

	"""
	An incomplete sample bot.

	:ivar summary_key: Edit summary message key. The message that should be
		used is placed on /i18n subdirectory. The file containing these
		messages should have the same name as the caller script (i.e. basic.py
		in this case). Use summary_key to set a default edit summary message.

	:type summary_key: str
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: 'pywikibot.page.BasePage') -> bool:
		# title = page.title()

		# page.protection()
		# if not page.has_permission():
		# 	pywikibot.warning(f"{title} is protected: this account can't edit it! Skipping...")
		# 	return True

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		page = self.current_page

		title = page.title()
		text = page.text
		# print(title)
		# print(text)

		code = mwparserfromhell.parse(text)

		template: mwparserfromhell.wikicode.Template = 1

		for template in code.filter_templates(
			matches=lambda x: x.name.matches('Cite news')
		):

			if not template.has('url') or not get_citation_fixer(template.get('url')):
				return

			if (template.has('last') or template.has('last1')) and not template.has('last2'):
				first_param = 'first'
				last_param = 'last'
				if template.has('last'):
					name = (get_val_safe(template, 'first') + ' ' + get_val_safe(template, 'last')).strip()
				if template.has('last1'):
					name = (get_val_safe(template, 'first1') + ' ' + get_val_safe(template, 'last1')).strip()
				if ',' in name:
					index = 0
					for name_inner in name.split(','):
						index += 1
						[last, first] = process_name(name_inner)
						template.add(last_param + str(index), last)
						template.add(first_param + str(index), first)
					template.add('first', '')
					template.remove('first')
					template.add('last', '')
					template.remove('last')

			if (template.has('editor-last') or template.has('editor-last1')) and not template.has('editor-last2'):
				first_param = 'editor-first'
				last_param = 'editor-last'
				if template.has('editor-last'):
					name = (get_val_safe(template, 'editor-first') + ' ' + get_val_safe(template, 'editor-last')).strip()
				if template.has('editor-last1'):
					name = (get_val_safe(template, 'editor-first1') + ' ' + get_val_safe(template, 'editor-last1')).strip()
				if ',' in name:
					index = 0
					for name_inner in name.split(','):
						index += 1
						[last, first] = process_name(name_inner)
						template.add(last_param + str(index), last)
						template.add(first_param + str(index), first)
					template.add('editor-first', '')
					template.remove('editor-first')
					template.add('editor-last', '')
					template.remove('editor-last')

			pywikibot.debug(template)

			text = str(code)
			# if is_dupe_fix: 
			# 	text = str(code)

		# print(text)
		# print(text_doc)

		self.put_current(text, summary=self.opt.summary, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary', 'text'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = MultiAuthorFixBot(generator=gen, **args)
		bot.run()  #

if __name__ == '__main__':
	main()
