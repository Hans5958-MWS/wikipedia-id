from mwparserfromhell.wikicode import Template
import re

def alias_fix(template: Template, info: dict):
	alias_to_convert = (
		('work', ('journal', 'newspaper', 'magazine', 'periodical', 'website')),
	)

	alias_to_convert_info = (
		*alias_to_convert,
		("last", ("last1",)),
		("first", ("first1",)),
		("editor-last", ("editor-last1",)),
		("editor-first", ("editor-first1",)),
	)

	alias_dupe_fix = (
		*alias_to_convert_info,
	)

	for (main_name, aliases) in alias_to_convert:
		for alias in aliases:
			if template.has(alias):
				template.get(alias).name = main_name

	# is_dupe_fix = False

	for (main_name, aliases) in alias_dupe_fix:
		for alias in aliases:
			if template.has(main_name):
				if template.has(alias):
					template.remove(alias)

	for (main_name, aliases) in alias_to_convert_info:
		for alias in aliases:
			if alias in info:
				alias_val = info.pop(alias)
				if not main_name in info:
					info[main_name] = alias_val
		if main_name in info:
			for alias in aliases:
				if template.has(alias):
					template.remove(alias)

	exclude_if_exists = (
		((r'^date\d*$', ), (r'^date\d*$', )),
		((r'^via\d*$', ), (r'^via\d*$', r'^source\d*$'))
	)

	for (searches, excludes) in exclude_if_exists:
		for search in searches:
			for param in template.params:
				# print(search, param.name)
				if re.match(search, str(param.name)):
					for info_key in list(info.keys()):
						for exclude in excludes:
							if re.match(exclude, info_key):
								# print(exclude, info_key)
								del info[info_key]
								break								
					break

	return (template, info)