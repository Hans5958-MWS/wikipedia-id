from mwparserfromhell.wikicode import Template
from difflib import SequenceMatcher

# https://stackoverflow.com/a/17388505
def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def get_names(template: Template):

	params = []
	authors = []
	name_rules = {}
	editors = []

	def set_names_special_treatment(names_special_treatment, name, key, value): 
		if name not in names_special_treatment:
			names_special_treatment[name] = {}
		names_special_treatment[name][key] = value

	for param in template.params:
		params.append(param.split('=')[0].strip())

	for param in params:
		if param.startswith('author') and 'first' not in param and 'last' not in param and 'link' not in param:
			name = str(template.get(param).value).strip()

			set_names_special_treatment(name_rules, name, 'merge', True)
			authors.append(name)

			link_param = param.replace('author', 'author-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

		elif param.startswith('author-last') or param.startswith('last'):
			last_param = param
			first_param = param.replace('last', 'first')
			name = str(template.get(last_param).value).strip()
			
			if first_param in params:
				name = str(template.get(first_param).value).strip() + " " + name
				authors.append(name)
			else:
				authors.append(name)
				
			if param.startswith('last'):
				link_param = param.replace('last', 'author-link')
			else:
				link_param = param.replace('-last', '-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

		elif param.startswith('author-first') or param.startswith('first'):
			first_param = param
			last_param = param.replace('first', 'last')
			if last_param in params:
				continue
			name = str(template.get(first_param).value).strip()
			
			authors.append(name)

			if param.startswith('first'):
				link_param = param.replace('first', 'author-link')
			else:
				link_param = param.replace('-first', '-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

		elif param.startswith('editor') and 'first' not in param and 'last' not in param and 'link' not in param:
			name = str(template.get(param).value).strip()

			set_names_special_treatment(name_rules, name, 'merge', True)
			editors.append(name)

			link_param = param.replace('editor', 'editor-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

		elif param.startswith('editor-last'):
			last_param = param
			first_param = param.replace('last', 'first')
			name = str(template.get(last_param).value).strip()
			
			if first_param in params:
				name = str(template.get(first_param).value).strip() + " " + name
				editors.append(name)
			else:
				editors.append(name)
				
			if param.startswith('last'):
				link_param = param.replace('last', 'editor-link')
			else:
				link_param = param.replace('-last', '-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

		elif param.startswith('editor-first'):
			first_param = param
			last_param = param.replace('first', 'last')
			if last_param in params:
				continue
			name = str(template.get(first_param).value).strip()
			editors.append(name)
			
			link_param = param.replace('-first', '-link')
			if link_param in params:
				link = str(template.get(link_param).value).strip()
				set_names_special_treatment(name_rules, name, 'link', link)

	# print(authors)
	authors = list(filter(lambda x: x is not None and x.strip() != "", list(dict.fromkeys(authors))))
	editors = list(filter(lambda x: x is not None and x.strip() != "", list(dict.fromkeys(editors))))
	# print(authors)

	return (authors, editors, name_rules)


def _split_name(name: str):
	name = name.strip()
	name_split = name.split(' ')
	if len(name_split) == 1:
		return ['', name]
	return [' '.join(name_split[0:-1]), name_split[-1]]

def set_names(template: Template, authors: list, editors: list, name_rules: dict):
	
	authors = list(filter(lambda x: x, list(dict.fromkeys(authors))))
	editors = list(filter(lambda x: x, list(dict.fromkeys(editors))))

	# print(authors, editors)

	# Hapus nama yang 70% sama (harusnya namanya cukup unik)
	def similar_filter(x, ref_list):
		result = filter(lambda y: similar(x.lower(), y.lower()) > 0.7, ref_list)
		return len(list(result)) == 0
	
	temp = []
	for name in authors:
		if similar_filter(name, temp):
			temp.append(name)
	authors = temp

	temp = []
	for name in editors:
		if similar_filter(name, temp):
			temp.append(name)
	editors = temp

	# print(authors, editors)

	for param in list(template.params):
		param = param.split('=')[0].strip()
		if param.startswith('author') or param.startswith('editor') or param.startswith('first') or param.startswith('last'):
			template.add(param, '')

	if len(authors) == 1:
		name = authors[0]
		if name in name_rules and name_rules[name]['merge']:
			template.add('author', name)
		else:
			[first, last] = _split_name(name)
			template.add('first', first)
			template.add('last', last)
		if name in name_rules and 'link' in name_rules[name]:
			link = name_rules[name]['link']
			template.add('author-link', link)
	else:
		for i in range(len(authors)):
			name = authors[i]
			i = str(i + 1)
			if (i == '1'):
				i = ''
			if name in name_rules and name_rules[name]['merge']:
				template.add('author' + i, name)
			else:
				[first, last] = _split_name(name)
				template.add('first' + i, first)
				template.add('last' + i, last)
			if name in name_rules and 'link' in name_rules[name]:
				# print(link)
				link = name_rules[name]['link']
				template.add('author-link' + i, link)
			
	if len(editors) == 1:
		name = editors[0]
		if name in name_rules and name_rules[name]['merge']:
			template.add('editor', name)
		else:
			[first, last] = _split_name(name)
			template.add('editor-first', first)
			template.add('editor-last', last)
		if name in name_rules and 'link' in name_rules[name]:
			link = name_rules[name]['link']
			template.add('editor-link', link)
	else:
		for i in range(len(editors)):
			name = editors[i]
			i = str(i + 1)
			if (i == '1'):
				i = ''
			if name in name_rules and name_rules[name]['merge']:
				template.add('editor' + i, name)
			else:
				[first, last] = _split_name(name)
				template.add('editor-first' + i, first)
				template.add('editor-last' + i, last)
			if name in name_rules and 'link' in name_rules[name]:
				link = name_rules[name]['link']
				template.add('author-link' + i, link)

	for param in list(template.params):
		param = param.split('=')[0].strip()
		# print(param)
		# print(param, str(template.get(param).value).strip(), str(template.get(param).value).strip() == '', param.startswith('author') or param.startswith('editor') or param.startswith('first') or param.startswith('last'))
		if param.startswith('author') or param.startswith('editor') or param.startswith('first') or param.startswith('last'):
			if str(template.get(param).value).strip() == '':
				template.remove(param)
