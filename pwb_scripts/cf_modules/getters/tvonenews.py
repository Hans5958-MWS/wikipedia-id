import json
from ._modules import CitationFixer, get_soup, process_name
from mwparserfromhell.wikicode import Template

class TvOneNewsCitationFixer(CitationFixer):
	name = "tvOneNews.com"
	
	"""
	Ambil judul, penulis, dan editor dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		# Ambil judul dan penulis dari situs
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			found = False
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if ld_json['@type'] == 'NewsArticle':
					found = True
					return_obj['title'] = ld_json['headline']
					# 0: Editor?
					# 1: Tim TvOne
					# 2: Penulis asli
					return_obj['_authors'].append(process_name(ld_json['author'][2]['name']))
					return_obj['_editors'].append(process_name(ld_json['author'][0]['name']))
			if not found:
				# Entah kenapa ada artikel yang tidak punya strucutred data-nya...
				# Juga ini biasanya tidak punya penulis (atau penulisnya hanya Tim TvOne dua-duanya), kalau tidak salah.
				return_obj["title"] = soup.find("meta", {"property": "og:title"}).attrs['content']

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[tvOne|tvOneNews.com]]'

		def name_filter(name):
			if 'viva' in name.lower():
				return False
			if 'antv' in name.lower():
				return False
			if 'tvone' in name.lower():
				return False
			return True

		return_obj['_authors'] = list(filter(name_filter, return_obj['_authors']))
		return_obj['_editors'] = list(filter(name_filter, return_obj['_editors']))
