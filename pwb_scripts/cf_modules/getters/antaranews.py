import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class AntaraNewsCitationFixer(CitationFixer):
	name = "ANTARA News"
	
	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
					# print(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		def name_filter(name):
			if 'antara' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		return_obj['work'] = '[[Lembaga Kantor Berita Nasional Antara|ANTARA News]]'

# https://riau.antaranews.com/berita/208028/wali-kota-dumai-resmilan-dua-kelurahan-baru-hasil-pemekaran