import json, re, requests
from ._modules import CitationFixer, get_soup, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class DetikComCitationFixer(CitationFixer):
	name = "Detik.com"
	
	"""
	Coba dapatkan URL baru dari URL format lama agar dapat diakses.
	"""
	def url(url: str):
		old_type_1 = re.search(r'.+\/index.php\/.+\/idnews\/(\d+)\/.+', url)
		if old_type_1:
			id = old_type_1.group(1)
			try:
				r = requests.get(f'https://news.detik.com/berita/d-{id}/_', timeout=5)
				return r.url
			except KeyboardInterrupt:
				raise KeyboardInterrupt()
			except:
				pass
		return url

	"""
	Ambil judul dan penulis dari situs jika tidak ada penulisnya.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Generalisasi nama situs.
	Biasanya Zotero sudah benar ambil judul, jadi ini tinggal memastikan.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'detik' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		return_obj['work'] = '[[Detik.com|detikcom]]'
		return_obj['publisher'] = ''
