import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json, get_val_safe
from mwparserfromhell.wikicode import Template

class MedcomCitationFixer(CitationFixer):
	name = "Medcom.id"
	
	"""
	Alihkan dari situs mobile.
	"""
	def url(old):
		old = str(old)
		return old.replace("://m.", "://www.")

	"""
	Buat judul di return_obj untuk diperbaiki di fungsi override.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		return_obj['title'] = get_val_safe(template, 'title')
		if return_obj['title'] == '':
			return_obj['title'] = '?'


	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				ld_json_result = get_from_ld_json(ld_json, return_obj)
				if ld_json_result:
					break

			# Author adalah editor di sini. (asumsi)
			if ld_json_result:
				editors = ld_json_result['authors']
				return_obj['_authors'] = list(filter(lambda x: x not in editors, return_obj['_authors']))
				return_obj['_editors'] += editors

			# Penulis sebenarnya
			info_ct = soup.select_one('.info_ct')
			if info_ct:
				name = '•'.join(info_ct.string.split('•')[:-1]).strip()
				if ',' in name:
					for name_inner in name.split(','):
						return_obj['_authors'].append(process_name(name_inner))
				else:
					return_obj['_authors'].append(process_name(name))

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['title'] = re.sub(r' - Medcom\.id', '', return_obj["title"])	
		return_obj['work'] = '[[Medcom.id]]'
