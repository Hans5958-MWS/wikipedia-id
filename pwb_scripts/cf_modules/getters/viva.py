import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, get_val_safe
from mwparserfromhell.wikicode import Template

class VivaCitationFixer(CitationFixer):
	name = "VIVA.co.id"
	
	"""
	Perbaiki judul, penulis, dan sumber.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		title = get_val_safe(template, 'title')
		if title.endswith(' - VIVA.co.id'):
			return_obj['title'] = title[6:-13]

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break
	"""
	Perbaiki penulis dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'viva' in name.lower():
				return False
			return True

		return_obj['_authors'] = list(filter(name_filter, return_obj['_authors']))
		return_obj['_editors'] = list(filter(name_filter, return_obj['_editors']))
		return_obj['work'] = '[[VIVA.co.id]]'
