import json
from ._modules import CitationFixer, get_soup, process_name, get_val_safe
from mwparserfromhell.wikicode import Template

class BisnisComCitationFixer(CitationFixer):
	name = "Bisnis.com"
	
	"""
	Alihkan news. ke kabar24..
	"""
	def url(old):
		new = str(old)
		new = old.replace("news.bisnis.com", "kabar24.bisnis.com")
		new = old.replace("m.bisnis.com/amp/", "bisnis.com/")
		new = old.replace("m.bisnis.com", "bisnis.com")
		return new

	"""
	Buat judul di return_obj untuk diperbaiki di fungsi override.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		return_obj['title'] = get_val_safe(template, 'title')
		if return_obj['title'] == '':
			return_obj['title'] = '?'

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if ld_json['@type'] == 'NewsArticle':
					return_obj['title'] = ld_json['headline']
					for author in ld_json['author']:
						if author['jobTitle'] == 'Editor':
							return_obj['_editors'].append(process_name(author['name']))
						else:
							return_obj['_authors'].append(process_name(author['name']))
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Bisnis Indonesia|Bisnis.com]]'
		if ' | ' in return_obj['title']:
			return_obj['title'] = ' | '.join(return_obj['title'].split(' | ')[0:-1])

