import json, re
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class WartaEkonomiCitationFixer(CitationFixer):
	name = "Warta Ekonomi"

	"""
	Ambil judul dan penulis dari situs
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			# Judul dan penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Warta Ekonomi]]'

# test = {}
# online('https://wartaekonomi.co.id/read453647/geger-berani-deklarasi-anies-baswedan-sebagai-calon-presiden-rocky-gerung-blak-blakan-sebut-nasdem-layak-ditendang-jokowi-dari-kabinet', None, test)
# print(test)