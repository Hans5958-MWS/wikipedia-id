import json, re
from ._modules import CitationFixer, get_soup, process_name, get_val_safe
from mwparserfromhell.wikicode import Template

class Liputan6CitationFixer(CitationFixer):
	name = "Liputan6.com"

	"""
	Alihkan m. ke www..
	"""
	def url(old):
		old = str(old)
		return old.replace("m.liputan6", "www.liputan6")

	"""
	Ambil judul, penulis, dan editor dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		# Ambil judul dan penulis dari situs
		soup = get_soup(url)

		if soup:
			found = False
			
			el = soup.select_one('[itemprop=headline]')
			if el:
				return_obj['title'] = el.string
			script_els = soup.find_all('script', {'type': 'text/javascript'})
			
			for el in script_els:
				if not el.string:
					continue
				match_reporter = re.compile(r'[\'"]reporters[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').search(el.string)

				if match_reporter:
					name = match_reporter.group(1)
					if ',' in name:
						for name_inner in name.split(','):
							return_obj['_authors'].append(process_name(name_inner))
					else:
						return_obj['_authors'].append(process_name(name))
					found = True

				match_editor = re.compile(r'[\'"]editors[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').search(el.string)

				if match_editor:
					name = match_editor.group(1)
					if ',' in name:
						for name_inner in name.split(','):
							return_obj['_editors'].append(process_name(name_inner))
					else:
						return_obj['_editors'].append(process_name(name))
					found = True

				if found:
					break

	"""
	Perbaiki bahasa, sumber, dan penulis.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'liputan6' in name.lower():
				return False
			return True
		
		return_obj['_authors'] = list(filter(name_filter, return_obj['_authors']))
		return_obj['_editors'] = list(filter(name_filter, return_obj['_editors']))

		return_obj['language'] = ''
		return_obj['work'] = '[[Liputan6.com]]'
