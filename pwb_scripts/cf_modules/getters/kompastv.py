import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json
from mwparserfromhell.wikicode import Template

class KompasTvCitationFixer(CitationFixer):
	name = "Kompas TV"

	"""
	Hapus /amp/.
	"""
	def url(old):
		old = str(old)
		return old.replace("/amp", "/")

	"""
	Ambil judul, penulis, dan editor dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		if soup:
		
			# Judul dan penulis.
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			# Editor
			editor = soup.find("meta", {"name": "content_editor"})
			if editor:

				name = editor.attrs['content']
				if ',' in name:
					for name_inner in name.split(','):
						return_obj['_editors'].append(process_name(name_inner))
				else:
					return_obj['_editors'].append(process_name(name))
	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Kompas TV]]'

# test = {}
# online("https://www.kompas.tv/article/344859/surat-terbuka-bos-mnc-group-hary-tanoe-protes-siaran-tv-digital-dan-akan-gugat-ke-pengadilan", None, test)
# print(test)

