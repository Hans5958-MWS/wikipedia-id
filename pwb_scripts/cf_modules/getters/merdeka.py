import re
from ._modules import CitationFixer, get_soup, process_name, filter_names
from mwparserfromhell.wikicode import Template

class MerdekaCitationFixer(CitationFixer):
	name = "Merdeka.com"
	"""
	Ambil judul, penulis, dan editor dari situs
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			found = False
			script_els = soup.find_all('script', {'type': 'text/javascript'})
			for el in script_els:
				if not el.string:
					continue
				match_title = re.compile(r'[\'"]articleTitle[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').search(el.string)
				if match_title:
					return_obj['title'] = match_title.group(1)
					found = True
				match_reporter = re.compile(r'[\'"]reporters[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').search(el.string)
				if match_reporter:
					return_obj['_authors'].append(process_name(match_reporter.group(1)))
					found = True
				match_editor = re.compile(r'[\'"]editors[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').search(el.string)
				if match_editor:
					return_obj['_editors'].append(process_name(match_reporter.group(1)))
					found = True
				if found:
					break

	"""
	Perbaiki penulis, bahasa, dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'merdeka' in name.lower():
				return False
			if 'viva' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		return_obj['language'] = ''
		return_obj['work'] = '[[Merdeka.com]]'


# test = {}
# online('https://www.merdeka.com/uang/harga-cabai-masih-mahal-bertahan-di-rp100000-per-kg.html', None, test)
# print(test)