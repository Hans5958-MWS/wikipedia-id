import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class OkezoneCitationFixer(CitationFixer):
	name = "Okezone"
	
	"""
	Perbaiki judul dan penulis.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		if template.has('title') and ' : Okezone' in template.get('title').value:
			return_obj['title'] = str(template.get('title').value).split(' : Okezone')[0]

	"""
	Ambil judul, penulis, dan editor dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		# Ambil judul dan penulis dari situs
		soup = get_soup(url)

		if soup:
			# Judul dan penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			# Editor (tersembunyi)
			script_els = soup.find_all('script')
			for el in script_els:
				if not el.string:
					continue
				match = re.compile('[\'"]editor[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').match(el.string)
				if match:
					return_obj['_editors'].append(process_name(match.group(1)))
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Okezone.com]]'

		def name_filter(name):
			if 'okezone' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)
