import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json
from mwparserfromhell.wikicode import Template

class JawaPosCitationFixer(CitationFixer):
	name = "JawaPos.com"
	
	"""
	Ambil judul, editor, dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				ld_json_result = get_from_ld_json(ld_json, return_obj)
				if ld_json_result:
					break

			# Author adalah editor di sini.
			if ld_json_result:
				editors = ld_json_result['authors']
				return_obj['_authors'] = list(filter(lambda x: x not in editors, return_obj['_authors']))
				return_obj['_editors'] += editors

			# Penulis sebenarnya di sini
			content_reporter = soup.find('div', {'class': 'content-reporter'})
			if content_reporter:
				authors = content_reporter.find_all('p')
				for author in authors:
					raw_str: str = author.string
					if "Editor" in raw_str:
						continue
					name = ' : '.join(raw_str.split(' : ')[1:])
					if ',' in name:
						for name_inner in name.split(','):
							return_obj['_authors'].append(process_name(name_inner))
					else:
						return_obj['_authors'].append(process_name(name))


	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Jawa Pos|JawaPos.com]]'
		if 'title' in return_obj and ' - Jawa Pos' in return_obj['title']:
			return_obj['title'] = ' - '.join(return_obj['title'].split(' - ')[0:-1])

# test = {}
# online("https://www.jawapos.com/bersama-lawan-covid-19/21/04/2022/pbb-bantu-185-ribu-orang-miskin-di-indonesia-terdampak-pandemi-covid/", None, test)
# print(test)
