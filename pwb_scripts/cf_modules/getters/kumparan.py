import json, re
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class KumparanCitationFixer(CitationFixer):
	name = "Kumparan"
	
	"""
	Ambil judul dan penulis dari situs
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			# Judul dan penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			# Tidak mungkin karena situs menggunakan React, yang memerlukan alat seperti Puppeter.
			# # Judul
			# return_obj["title"] = soup.find("meta", {"property": "og:title"}).attrs['content']

			# # Penulis dan editor
			# author_index = 0
			# editor_index = 0
			
			# print(soup.select_one('.jYZaMJ'))

			# for el in soup.select('.jYZaMJ > div:nth-child(1) > div'):
			# 	name = el.select_one('.dYTzfs a').string.strip()
			# 	role = el.select_one('.lhmknV').string.strip()
			# 	if role == "Writer":
			# 		author_index += 1
			# 		[return_obj["last" + str(author_index)], return_obj["first" + str(author_index)]] = separate_name(name)
			# 	elif role == "Editor":
			# 		editor_index += 1
			# 		[return_obj["editor-last" + str(editor_index)], return_obj["editor-first" + str(editor_index)]] = separate_name(name)

	# test = {}
	# online('https://kumparan.com/kumparannews/foto-kerusakan-akibat-kebakaran-hutan-imbas-gelombang-panas-di-prancis-1yWb9B6rn6r/full', None, test)
	# print(test)

	"""
	Perbaiki sumber dan penulis.
	"""
	def override(url: str, template: Template, return_obj: dict):
		def name_filter(name):
			if 'kumparan' in name.lower():
				return False
			return True
		
		return_obj['_authors'] = list(filter(name_filter, return_obj['_authors']))
		return_obj['_editors'] = list(filter(name_filter, return_obj['_editors']))

		return_obj['work'] = '[[Kumparan (situs web)|Kumparan]]'

