import re
from bs4 import BeautifulSoup
import requests
import urllib3
from dateutil import parser
from mwparserfromhell.wikicode import Template

def get_soup(url: str):
	url = url.strip()
	try:
		response = requests.get(url, timeout=5)
	except KeyboardInterrupt:
		raise KeyboardInterrupt()
	except:
		urllib3.disable_warnings()
		response = requests.get(url, verify=False, timeout=5)
	if response.ok:
		return BeautifulSoup(response.content, features="html.parser")
	return None

def process_name(name: str):
	if not name:
		return None
	name = name.strip()
	name = re.sub(r'\s*([Kk]ontributor)\s*', '', name)
	if re.compile('^-+$').search(name):
		return None
	if name.upper() == name or name.lower() == name:
		name = name.title()
	return name

def filter_names(return_obj: object, name_filter):
	return_obj['_authors'] = list(filter(name_filter, return_obj['_authors']))
	return_obj['_editors'] = list(filter(name_filter, return_obj['_editors']))

def get_from_ld_json(ld_json: object, return_obj: object):

	authors = []
	editors = []

	if ld_json.get('@type', None) == 'NewsArticle':
		return_obj['title'] = ld_json['headline']

		if 'author' in ld_json:
			if isinstance(ld_json['author'], list):
				for el in ld_json['author']:
					name = el['name'] or ''
					if ',' in name:
						for name_inner in name.split(','):
							authors.append(process_name(name_inner))
					else:
						authors.append(process_name(name))
			else:
				name = ld_json['author']['name'] or ''
				if ',' in name:
					for name_inner in name.split(','):
						authors.append(process_name(name_inner))
				else:
					authors.append(process_name(name))

		if 'editor' in ld_json:
			if isinstance(ld_json['editor'], list):
				for el in ld_json['editor']:
					name = el['name'] or ''
					if ',' in name:
						for name_inner in name.split(','):
							editors.append(process_name(name_inner))
					else:
						editors.append(process_name(name))
			else:
				name = ld_json['editor']['name'] or ''
				if ',' in name:
					for name_inner in name.split(','):
						editors.append(process_name(name_inner))
				else:
					editors.append(process_name(name))
		
		if 'datePublished' in ld_json:
			date = parser.parse(ld_json['datePublished'])
			return_obj['date'] = date.strftime('%Y-%m-%d')

		return_obj['_authors'] += authors
		return_obj['_editors'] += editors
		return {
			'authors': authors,
			'editors': editors
		}
	return False

def get_val_safe(template: Template, param: str):
	if template.has(param):
		return str(template.get(param).value)
	return ''

class CitationFixer():
	name: None | str = ''
	skip: bool = False

	def url(str) -> str:
		pass

	def online(url: str, template: Template, return_obj: dict):
		pass

	def offline(url: str, template: Template, return_obj: dict):
		pass

	def override(url: str, template: Template, return_obj: dict):
		pass
	