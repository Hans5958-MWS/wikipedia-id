import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class KontanCitationFixer(CitationFixer):
	name = "Kontan"
	
	"""
	Alihkan amp. ke www..
	"""
	def url(old):
		old = str(old)
		return old.replace("amp.", "www.")

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:

			# Batal jika 404
			og_url = soup.find("meta", {"property": "og:url"})
			if og_url and ("404" in og_url.get('content')):
				return

			# Penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break
				
			# Editor (tersembunyi)
			editor = soup.find("meta", {"name": "content_editor"})
			if editor:
				return_obj['_editors'].append(editor.attrs['content'])

			# Judul
			return_obj["title"] = soup.find('h1').string

	"""
	Perbaiki bahasa dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		def name_filter(name):
			if 'kontan' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		return_obj['language'] = ''
		return_obj['work'] = '[[Kontan|Kontan.co.id]]'


