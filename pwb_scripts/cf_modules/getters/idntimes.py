import json
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class IdnTimesCitationFixer(CitationFixer):
	name = "IDN Times"

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki bahasa dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[IDN Times]]'

# test = {}
# online("https://www.idntimes.com/news/world/dwifantya-aquina/13-orang-tewas-dalam-kebakaran-kelab-malam-di-rusia", None, test)
# print(test)