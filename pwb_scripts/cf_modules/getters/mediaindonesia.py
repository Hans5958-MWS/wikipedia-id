from ._modules import CitationFixer, get_soup
from mwparserfromhell.wikicode import Template

class MediaIndonesiaCitationFixer(CitationFixer):
	name = "Media Indonesia"
	
	"""
	Ambil judul dari situs.
	(Sepertinya komponen-komponen lain tidak teralu reliable...)
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			title = soup.find('h1')
			if title:
				return_obj['title'] = soup.find('h1').string

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Media Indonesia]]'
