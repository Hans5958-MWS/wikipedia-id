import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json, get_val_safe, filter_names
from mwparserfromhell.wikicode import Template

class KompasCitationFixer(CitationFixer):
	name = "Kompas.com"

	"""
	Alihkan amp. ke www..
	"""
	def url(old):
		old = str(old)
		return old.replace("amp.", "www.")

	"""
	Buat judul di return_obj untuk diperbaiki di fungsi override.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		return_obj['title'] = get_val_safe(template, 'title')
		if return_obj['title'] == '':
			return_obj['title'] = '?'

	"""
	Ambil judul dan penulis dari situs dan perbaiki nama situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		if soup:

			# Judul dan editor.
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				ld_json_result = get_from_ld_json(ld_json, return_obj)
				if ld_json_result:
					break

			# Author adalah editor di sini.
			if ld_json_result:
				editors = ld_json_result['authors']
				return_obj['_authors'] = list(filter(lambda x: x not in editors, return_obj['_authors']))
				return_obj['_editors'] += editors

			# Penulis
			author = soup.find("meta", {"name": "content_author"})
			if author:

				name = author.attrs['content']
				name = re.sub(r'Kontributor( [a-zA-Z ]*?)?, ', '', name)
				
				if ',' in name:
					for name_inner in name.split(','):
						return_obj['_authors'].append(process_name(name_inner))
				else:
					return_obj['_authors'].append(process_name(name))

			print(return_obj)

	"""
	Perbaiki sumber dan penulis.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'kompas' in name.lower() or 'cyber media' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		if 'halaman all' in return_obj['title'].lower():
			return_obj['title'] = re.sub(r'\s*halaman all.*', '', return_obj['title'], flags=re.IGNORECASE)

		return_obj['work'] = '[[Kompas.com]]'

# test = {}
# online("http://tekno.kompas.com/read/2022/11/04/14150027/ramai-pengguna-bagikan-total-transaksi-gojek-tembus-ratusan-juta-begini-cara", None, test)
# print(test)