import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class CNNIndonesiaCitationFixer(CitationFixer):
	name = "CNN Indonesia"
	
	"""
	Ambil judul (dan coba ambil penulis) dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki penulis dan sumber.
	Biasanya Zotero sudah benar ambil judul, jadi ini tinggal memastikan.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'cnn' in name.replace('. ', '').lower() and 'indonesia' in name.lower():
				return False
			if 'tim' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)
			
		return_obj['work'] = '[[CNN Indonesia]]'
