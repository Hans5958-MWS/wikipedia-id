import json, re
from ._modules import CitationFixer, get_soup, process_name
from mwparserfromhell.wikicode import Template
import random
import string

class TirtoCitationFixer(CitationFixer):
	name = "Tirto.id"

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		temp_inter = ''.join(random.choice(string.ascii_letters) for i in range(4))

		if soup:
			# Sebearnya ada strucutred data pada Tirto, tapi tidak digunakan karena penulis dan editor digabung

			# Judul
			return_obj["title"] = soup.find("meta", {"property": "og:title"}).attrs['content']

			# Penulis dan editor
			datalayer = soup.find("script", {"data-hid": "dataLayer_event"})
			if datalayer:
				datalayer = datalayer.string
				datalayer = re.search(r'\{.+\}', datalayer, re.DOTALL + re.MULTILINE)[0]
				datalayer = re.sub(r',\s*}\s*$', '}', datalayer).replace("\"", temp_inter).replace("\'", "\"").replace(temp_inter, "\\\"")
				datalayer: dict = json.loads(datalayer)
				if 'authorPenulis' in datalayer:
					return_obj['_authors'].append(process_name(datalayer['authorPenulis']))
				if 'authorKontributor' in datalayer:
					return_obj['_authors'].append(process_name(datalayer['authorKontributor']))
				if 'authorEditor' in datalayer:
					return_obj['_editors'].append(process_name(datalayer['authorEditor']))

	"""
	Perbaiki bahasa dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['language'] = ''
		return_obj['work'] = '[[Tirto|Tirto.id]]'

# return_obj = {
# 	'_authors': [],
# 	'_editors': [],
# }
# online('https://tirto.id/bahasa-bukan-satu-satunya-manfaat-metode-read-aloud-pada-anak-gy1S', None, return_obj)
# online('https://tirto.id/apa-saja-ciri-ciri-awal-kehamilan-kenali-tanda-tandanya-gy1g', None, return_obj)
# print(return_obj)
