import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template

class JpnnCitationFixer(CitationFixer):
	name = "JPNN.com"
	
	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Jawa Pos|JPNN.com]]'

		def name_filter(name):
			if 'redaksi' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)
