import json, re
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class KompasIdCitationFixer(CitationFixer):
	name = "Kompas.id"
	
	"""
	Tentukan tipe akses (apakah bersifat memerlukan langganan)
	"""
	def offline(url: str, template: Template, return_obj: dict):
		if "/bebas-akses/" in url:
			return_obj['url-access'] = "free"
		else:
			return_obj['url-access'] = "subscription"

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Kompas (surat kabar)|Kompas.id]]'

# test = {
# 	"_authors": [],
# 	"_editors": []
# }
# online("https://www.kompas.id/baca/ekonomi/2023/01/04/sana-sini-heboh-mixue", None, test)
# print(test)
