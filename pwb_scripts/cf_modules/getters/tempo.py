import json
from ._modules import CitationFixer, get_soup, get_from_ld_json, get_val_safe, filter_names
from mwparserfromhell.wikicode import Template

class TempoCitationFixer(CitationFixer):
	name = "Tempo.co"
	
	"""
	Alihkan /amp ke /read/.
	"""
	def url(old):
		old = str(old)
		return old.replace("/amp/", "/read/")

	"""
	Buat judul di return_obj untuk diperbaiki di fungsi override.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		return_obj['title'] = get_val_safe(template, 'title')
		if return_obj['title'] == '':
			return_obj['title'] = '?'
		return_obj['url-access'] = ''

	"""
	Ambil judul dan penulis dari situs dan tentukan tipe akses.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		old_title = return_obj.get('title')

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			if return_obj['title'] == 'Koran TEMPO | Situs Berita Online Indonesia':
				return_obj['title'] = old_title

			if "koran.tempo" in url or "majalah.tempo" in url:
				keyword_el = soup.find('meta', {'name': 'keywords'})
				if keyword_el and "Bebas Akses" in keyword_el['content']:
					return_obj['url-access'] = "free"
				else:
					return_obj['url-access'] = "subscription"

	"""
	Perbaiki bahasa, sumber, dan judul.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			# print(name)
			if 'tempo' in name.lower():
				return False
			if 'administrator' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		# 404
		while 'Koran TEMPO' in return_obj['title'] and 'Situs Berita' in return_obj['title']:
			del return_obj['title']

		return_obj['language'] = ''
		return_obj['work'] = '[[Tempo.co]]'

# test = {
# 	"_authors": [],
# 	"_editors": []
# }
# online("https://koran.tempo.co/read/cover-story/479271/setelah-badai-urung-datang-di-jakarta", None, test)
# print(test)
