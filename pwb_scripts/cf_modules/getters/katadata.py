import json, re
from ._modules import CitationFixer, get_soup, get_from_ld_json, get_val_safe
from mwparserfromhell.wikicode import Template


class KatadataCitationFixer(CitationFixer):
	name = "Katadata"
	
	"""
	Buat judul di return_obj untuk diperbaiki di fungsi override.
	"""
	def offline(url: str, template: Template, return_obj: dict):
		return_obj['title'] = get_val_safe(template, 'title')
		if return_obj['title'] == '':
			return_obj['title'] = '?'

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki judul dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Katadata]]'
		return_obj['title'] = re.compile(' -[^-]*Katadata.co.id[^-]*$').sub('', return_obj['title'])
