import json
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class PikiranRakyatCitationFixer(CitationFixer):
	name = "Pikiran-Rakyat.com"
	
	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			title = soup.find('h1')
			if title:
				return_obj['title'] = soup.find('h1').string

	"""
	Perbaiki sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Pikiran Rakyat|Pikiran-Rakyat.com]]'

# test = {}
# online("https://www.pikiran-rakyat.com/ekonomi/pr-015795008/cerita-tentang-kawung-keretek-dan-kejayaan-rokok-lokal-sebelum-digulung-sigaret-impor", None, test)
# print(test)
