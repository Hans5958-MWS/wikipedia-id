import json, re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json
from mwparserfromhell.wikicode import Template

class SuaraCitationFixer(CitationFixer):
	name = "Suara.com"
	
	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			# Judul dan penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			# Editor (tersembunyi)
			script_els = soup.find_all('script')
			for el in script_els:
				if not el.string:
					continue
				match = re.compile('[\'"]articleEditor[\'"]\s*:\s*[\'"]([^\'"]+?)[\'"]').match(el.string)
				if match:
					return_obj['_editors'].append(process_name(match.group(1)))
					break

	"""
	Perbaiki bahasa dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['language'] = ''
		return_obj['work'] = 'Suara.com'
