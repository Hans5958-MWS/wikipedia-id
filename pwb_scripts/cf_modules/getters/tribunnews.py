import json
import re
from ._modules import CitationFixer, get_soup, process_name, get_from_ld_json, filter_names
from mwparserfromhell.wikicode import Template
import random
import string

class TribunnewsCitationFixer(CitationFixer):
	name = "Tribunnews"
	
	"""
	Alihkan dari situs mobile dan AMP.
	"""
	def url(old):
		old = str(old)
		return old.replace("://m.", "://www.").replace("/amp", "")

	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)
		temp_inter = ''.join(random.choice(string.ascii_letters) for i in range(4))

		if soup:

			# Penulis
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

			# Penulis (lagi) dan editor
			ld_json_els = soup.find_all('script')
			datalayer = ''
			for el in ld_json_els:
				if el.string and 'dataLayer' in el.string:
					datalayer = el.string
					break

			if datalayer:
				datalayer = re.search(r'\{.+\}', datalayer, re.DOTALL + re.MULTILINE)[0]
				datalayer = re.sub(r',\s*}\s*$', '}', datalayer).replace("\"", temp_inter).replace("\'", "\"").replace(temp_inter, "\\\"")
				datalayer: dict = json.loads(datalayer)
				if 'penulis' in datalayer:
					return_obj['_authors'].append(process_name(datalayer['penulis']))
				if 'editor' in datalayer:
					return_obj['_editors'].append(process_name(datalayer['editor']))

			# Editor
			divs = soup.find('div', {'id': 'editor'})
			if divs:
				for el in divs.find_all('a'):
					return_obj['_editors'].append(process_name(el.string))

	"""
	Perbaiki bahasa dan sumber.
	"""
	def override(url: str, template: Template, return_obj: dict):

		def name_filter(name):
			if 'tribunnews' in name.lower():
				return False
			return True
		filter_names(return_obj, name_filter)

		return_obj['language'] = ''
		return_obj['work'] = '[[Tribunnews|Tribunnews.com]]'

# return_obj = {}
# online('https://www.tribunnews.com/nasional/2022/07/24/kapolres-jaksel-dinon-aktifkan-kasus-brigadir-j-pengamat-janggal-kalau-kapolda-metro-jaya-tidak', None, return_obj)
# print(return_obj)
