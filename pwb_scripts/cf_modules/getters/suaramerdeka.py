import json
import re
from ._modules import CitationFixer, get_soup, get_from_ld_json
from mwparserfromhell.wikicode import Template

class SuaraMerdekaCitationFixer(CitationFixer):
	name = "Suara Merdeka Online"
	
	"""
	Ambil judul dan penulis dari situs.
	"""
	def online(url: str, template: Template, return_obj: dict):
		soup = get_soup(url)

		if soup:
			ld_json_els = soup.find_all('script', {"type": "application/ld+json"})
			for el in ld_json_els:
				try:
					ld_json = json.loads(el.string)
				except:
					continue
				if get_from_ld_json(ld_json, return_obj):
					break

	"""
	Perbaiki sumber dan judul.
	"""
	def override(url: str, template: Template, return_obj: dict):
		return_obj['work'] = '[[Suara Merdeka|Suara Merdeka Online]]'
		if 'title' in return_obj and '- suara merdeka' in return_obj['title'].lower():
			return_obj['title'] = re.sub(r'\s*- suara merdeka.*', '', return_obj['title'], flags=re.IGNORECASE)

# test = {}
# online("https://www.suaramerdeka.com/bola/pr-045468559/prediksi-skor-big-match-chelsea-vs-arsenal-6-november-2022-demi-harga-diri-derby-london", None, test)
# print(test)
