import re

from pwb_scripts.cf_modules.getters._modules import CitationFixer
from pwb_scripts.cf_modules.getters.antaranews import AntaraNewsCitationFixer
from pwb_scripts.cf_modules.getters.bisniscom import BisnisComCitationFixer
from pwb_scripts.cf_modules.getters.cnbcindonesia import CNBCIndonesiaCitationFixer
from pwb_scripts.cf_modules.getters.cnnindonesia import CNNIndonesiaCitationFixer
from pwb_scripts.cf_modules.getters.detikcom import DetikComCitationFixer
from pwb_scripts.cf_modules.getters.idntimes import IdnTimesCitationFixer
from pwb_scripts.cf_modules.getters.jawapos import JawaPosCitationFixer
from pwb_scripts.cf_modules.getters.jpnn import JpnnCitationFixer
from pwb_scripts.cf_modules.getters.katadata import KatadataCitationFixer
from pwb_scripts.cf_modules.getters.kompas import KompasCitationFixer
from pwb_scripts.cf_modules.getters.kompasid import KompasIdCitationFixer
from pwb_scripts.cf_modules.getters.kompastv import KompasTvCitationFixer
from pwb_scripts.cf_modules.getters.kontan import KontanCitationFixer
from pwb_scripts.cf_modules.getters.kumparan import KumparanCitationFixer
from pwb_scripts.cf_modules.getters.liputan6 import Liputan6CitationFixer
from pwb_scripts.cf_modules.getters.medcom import MedcomCitationFixer
from pwb_scripts.cf_modules.getters.mediaindonesia import MediaIndonesiaCitationFixer
from pwb_scripts.cf_modules.getters.merdeka import MerdekaCitationFixer
from pwb_scripts.cf_modules.getters.metrotvnews import MetrotvnewsCitationFixer
from pwb_scripts.cf_modules.getters.okezone import OkezoneCitationFixer
from pwb_scripts.cf_modules.getters.pikiranrakyat import PikiranRakyatCitationFixer
from pwb_scripts.cf_modules.getters.sindonews import SindonewsCitationFixer
from pwb_scripts.cf_modules.getters.suara import SuaraCitationFixer
from pwb_scripts.cf_modules.getters.suaramerdeka import SuaraMerdekaCitationFixer
from pwb_scripts.cf_modules.getters.swa import SwaCitationFixer
from pwb_scripts.cf_modules.getters.tempo import TempoCitationFixer
from pwb_scripts.cf_modules.getters.tirto import TirtoCitationFixer
from pwb_scripts.cf_modules.getters.tribunnews import TribunnewsCitationFixer
from pwb_scripts.cf_modules.getters.tvonenews import TvOneNewsCitationFixer
from pwb_scripts.cf_modules.getters.viva import VivaCitationFixer
from pwb_scripts.cf_modules.getters.wartaekonomi import WartaEkonomiCitationFixer

RE_DICT = {
	# VIVA
	'https?:\/\/[^/]*\.viva\.co\.id\/': VivaCitationFixer,
	'https?:\/\/[^/]*\.tvonenews\.com\/': TvOneNewsCitationFixer,
	# detik Network (Trans Media)
	'https?:\/\/[^/]*\.detik\.com\/': DetikComCitationFixer,
	'https?:\/\/[^/]*\.cnnindonesia\.com\/': CNNIndonesiaCitationFixer,
	'https?:\/\/[^/]*\.cnbcindonesia\.com\/': CNBCIndonesiaCitationFixer,
	# MNC Portal (MNC Media)
	'https?:\/\/[^/]*\.okezone\.com\/': OkezoneCitationFixer,
	'https?:\/\/[^/]*\.sindonews\.com\/': SindonewsCitationFixer,
	# KapanLagi Youniverse (SCM/Emtek)
	'https?:\/\/[^/]*\.liputan6\.com\/': Liputan6CitationFixer,
	'https?:\/\/[^/]*\.merdeka\.com\/': MerdekaCitationFixer,
	# KG Media (Kompas)
	'https?:\/\/[^/]*\.kompas\.com\/': KompasCitationFixer,
	'https?:\/\/[^/]*\.kompas\.id\/': KompasIdCitationFixer,
	'https?:\/\/[^/]*\.kompas\.tv\/': KompasTvCitationFixer,
	'https?:\/\/[^/]*\.tribunnews\.com\/': TribunnewsCitationFixer,
	'https?:\/\/[^/]*\.kontan\.co\.id\/': KontanCitationFixer,
	# Metro
	'https?:\/\/[^/]*\.metrotvnews\.com\/': MetrotvnewsCitationFixer,
	'https?:\/\/[^/]*\.mediaindonesia\.com\/': MediaIndonesiaCitationFixer,
	'https?:\/\/[^/]*\.medcom\.id\/': MedcomCitationFixer,
	# Individual
	'https?:\/\/[^/]*\.idntimes\.com\/': IdnTimesCitationFixer,
	'https?:\/\/[^/]*\.jpnn\.com\/': JpnnCitationFixer,
	'https?:\/\/[^/]*\.jawapos\.com\/': JawaPosCitationFixer,
	'https?:\/\/[^/]*\.tempo\.co\/': TempoCitationFixer,
	'https?:\/\/[^/]*\.swa\.co\.id\/': SwaCitationFixer,
	'https?:\/\/[^/]*\.kumparan\.com\/': KumparanCitationFixer,
	'https?:\/\/[^/]*\.antaranews\.com\/': AntaraNewsCitationFixer,
	'https?:\/\/[^/]*\.tirto\.id\/': TirtoCitationFixer,
	'https?:\/\/[^/]*\.katadata\.co.id\/': KatadataCitationFixer,
	'https?:\/\/[^/]*\.bisnis\.com\/': BisnisComCitationFixer,
	'https?:\/\/[^/]*\.suara\.com\/': SuaraCitationFixer,
	'https?:\/\/[^/]*\.wartaekonomi\.co.id\/': WartaEkonomiCitationFixer,
	'https?:\/\/[^/]*\.suaramerdeka\.com\/': SuaraMerdekaCitationFixer,
	'https?:\/\/[^/]*\.pikiran-rakyat\.com\/': PikiranRakyatCitationFixer,
	# 'https?:\/\/[^/]*\.indonesiatoday\.co.id\/': IndonesiaTodayCitationFixer, # TODO
}

"""
Memperoleh CitationFixer perusahaan berita/situs berita sesuai dengan URL yang diberikan.
"""
def get_citation_fixer(url: str) -> CitationFixer | None:
	if not url:
		return None
	for regexp_text in RE_DICT.keys():
		if re.compile(regexp_text).search(str(url)):
			return RE_DICT[regexp_text]
	return None