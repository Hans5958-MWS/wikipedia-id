import html
import importlib
import importlib.util
import json, re
import traceback
from bs4 import BeautifulSoup
from pwb_scripts.cf_modules.get_company import get_citation_fixer
# from get_company import get_company
import requests
from pywikibot import critical, debug, error, info, warning
from urllib3.exceptions import ReadTimeoutError
from requests.exceptions import ReadTimeout, ConnectionError, ConnectTimeout, TooManyRedirects
from mwparserfromhell.wikicode import Template
from pwb_scripts.cf_modules.name_processor import get_names
class GetterUndefinedError(Exception):
	pass

def _clean_return_obj(return_obj: dict):
	for key in list(return_obj.keys()):
		if return_obj[key] is None:
			return_obj[key] = ""
		return_obj[key] = html.unescape(return_obj[key])
		if return_obj[key] == "?":
			return_obj.pop(key)
		if key == '_authors' or key == '_editors':
			return_obj[key] = list(filter(lambda x: x, return_obj[key]))

def _get_val_safe(template: Template, param: str):
	if template.has(param):
		return str(template.get(param).value)
	return ''

def _get_dict_val_safe(dict: dict, param: str):
	if param in dict:
		return dict[param]
	return ''

def get_info(url: str, template: Template):
	is_minor = 0

	url = str(url)
	status_text = "OK"
	citation_fixer = get_citation_fixer(url)
	
	if citation_fixer:
		debug('Memperoleh data...')
		debug(f'Company: {citation_fixer.name}')
		debug(f'URL: {url}')
	else:
		return None
	
	return_obj = {}

	[authors, editors, name_rules] = get_names(template)

	return_obj['_authors'] = authors[:]
	return_obj['_editors'] = editors[:]
	return_obj['_name_rules'] = name_rules.copy()

	if citation_fixer.skip:
		debug('Getter tidak terdefinisi pada situs ini. Melewati.')
		status_text = "SKIP"
		info(f"| {citation_fixer.ljust(16)[:16]} | {url.ljust(64)[:64]} | {status_text.ljust(8)} |")
		return {}
		
	new_url = citation_fixer.url(url) or url
	if new_url != url:
		debug('Memperbaiki URL...')
		return_obj['url'] = url
		debug(f'URL: {url}')

	debug('Memperbaiki tanpa berhubung ke situs (jika ada)...')
	citation_fixer.offline(url, template, return_obj)

	_clean_return_obj(return_obj)

	try:
		debug('Mencoba mendapatkan info dari situs (jika ada)...')
		citation_fixer.online(url, template, return_obj)
	except ReadTimeoutError:
		debug('Sumber kelamaan untuk menjawab. Lompati!')
		status_text = "SKIP"
	except ReadTimeout:
		debug('Sumber kelamaan untuk menjawab. Lompati!')
		status_text = "SKIP"
	except ConnectTimeout:
		debug('Sumber kelamaan untuk menjawab. Lompati!')
		status_text = "SKIP"
	except ConnectionError:
		debug('Sumber tidak dapat diperoleh. Lompati!')
		status_text = "SKIP"
	except TooManyRedirects:
		debug('Sumber \"lempar masalah\" terus (kebanyakan redirect). Lompati!')
		status_text = "SKIP"
	except KeyboardInterrupt:
		raise KeyboardInterrupt()
	except:
		error('Masalah terjadi! Mohon diperiksa!')
		error(traceback.format_exc())
		status_text = "FAIL    "

	_clean_return_obj(return_obj)

	debug('Melakukan perlakuan akhir (jika ada)...')
	citation_fixer.override(url, template, return_obj)

	# print(return_obj)

	_clean_return_obj(return_obj)

	if authors == return_obj['_authors'] and editors == return_obj['_editors']:
		del return_obj['_authors']
		del return_obj['_editors']
		del return_obj['_name_rules']

	if "id" in _get_val_safe(template, 'language'):
		return_obj['language'] = ''

	info(f"| {citation_fixer.name.ljust(16)[:16]} | {url.ljust(64)[:64]} | {status_text.ljust(8)} |")

	return (return_obj, is_minor > 0)

# test = get_info("https://www.kompas.com/parapuan/read/533389252/jelang-hari-anak-nasional-ini-profil-pecatur-cilik-samantha-edithso")
# test = get_info("https://www.viva.co.id/berita/nasional/1500895-pengacara-brigadir-j-ada-petinggi-besar-menyuruh-ambil-recorder-cctv")
# print(test)