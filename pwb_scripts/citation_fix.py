#!/usr/bin/python3
"""
Memperbarui referensi dari situs berita Indonesia.

Use global -simulate option for test purposes. No changes to live wiki
will be done.

The following parameters are supported:

-always		   The bot won't ask for confirmation when putting a page

-summary:		 Set the action summary message for the edit.

Every script has its own section with the script name as header.

In addition the following generators and filters are supported but
cannot be set by settings file:

&params;
"""
import re
import pywikibot
from pywikibot import pagegenerators
from pywikibot.bot import (
	AutomaticTWSummaryBot,
	ConfigParserBot,
	ExistingPageBot,
	SingleSiteBot,
)
from pywikibot.page import Page, Revision
import mwparserfromhell
import pywikibot
from datetime import datetime, timedelta
from pwb_scripts.cf_modules.alias_fix import alias_fix
from pwb_scripts.cf_modules.get_company import get_citation_fixer
from pwb_scripts.cf_modules.name_processor import set_names
from pwb_scripts.cf_modules.info_getter import get_info

docuReplacements = {'&params;': pagegenerators.parameterHelp}

DEFAULT_ARGS = {
	'summary': '🤖 Perbarui referensi situs berita Indonesia',
	'minor': True,
	'force': False
}

def cite_template_match(template: mwparserfromhell.wikicode.Template):
	for name in ["Cite news", "Cite-news", "Cite article", "Cite newspaper", "Cit news", "Cite new", "Mengutip berita"]:
		if template.name.matches(name):
			return 1
	for name in ["Cite web", "Web reference", "Cite-web", "Citeweb", "Referensi web", "Cite website", "Web cite", "Memetik web", "Cite blog", "Cita web", "Web", "Citar web", "Cite webpage", "Cite web.", "Chú thích web", "Cite url"]:
		if template.name.matches(name):
			return 2
		
# Regex that match bare references
linksInRef = re.compile(
	# bracketed URLs
	r'(?i)<ref(?P<name>[^>]*)>\s*\[?(?P<url>(?:http|https)://(?:'
	# unbracketed with()
	r'^\[\]\s<>"]+\([^\[\]\s<>"]+[^\[\]\s\.:;\\,<>\?"]+|'
	# unbracketed without ()
	r'[^\[\]\s<>"]+[^\[\]\s\)\.:;\\,<>\?"]+|[^\[\]\s<>"]+))'
	r'[!?,\s]*\]?\s*</ref>')

class CitationFixerBot(
	SingleSiteBot,
	ConfigParserBot,
	ExistingPageBot,
	AutomaticTWSummaryBot,
):

	"""
	TBA

	:ivar summary_key: Edit summary message key. The message that should be
		used is placed on /i18n subdirectory. The file containing these
		messages should have the same name as the caller script (i.e. basic.py
		in this case). Use summary_key to set a default edit summary message.

	:type summary_key: str
	"""

	use_redirects = False  # treats non-redirects only
	update_options = DEFAULT_ARGS

	def skip_page(self, page: Page) -> bool:

		title = page.title()
		text = page.text
		code = mwparserfromhell.parse(text)
		
		page.protection()
		if not page.has_permission():
			pywikibot.info(f'Dilompati: {title}: Halaman dilindungi.')
			return True
		
		if not code.filter_templates(matches=lambda template: 
			cite_template_match(template) and
			get_citation_fixer(template.get('url', None))
		) and not list(filter(lambda x: get_citation_fixer(x[1]), linksInRef.findall(text))):
			pywikibot.info(f'Dilompati: {title}: Tidak ada templat referensi untuk diedit.')
			return True

		if not self.opt.force:

			last_revisions = list(page.revisions(total=10))
			revision: Revision = None
			last_self_rev: Revision = None
			last_self_rev_index = -1
			last_other_rev: Revision = None
			last_other_rev_index = -1
			index = -1
			for revision in last_revisions:
				index += 1
				if revision.get('user') == "Bot5958":
					if not last_self_rev:
						last_self_rev = revision
						last_self_rev_index = index
				else:
					if not last_other_rev:
						last_other_rev = revision
						last_other_rev_index = index
				delta: timedelta = datetime.now() - revision.get('timestamp')
				# print(delta.days)

			if not page.botMayEdit() or code.filter_templates(matches=lambda x: (
				x.name.matches('In use') or 
				x.name.matches('Inuse') or 
				x.name.matches('Inusefor') or 
				x.name.matches('Inuseuntil') or 
				x.name.matches('Sedang ditulis')
			)):
				pywikibot.info(f'Dilompati: {title}: Halaman tidak ingin disuting oleh bot.')
				return True 

			title = page.title()
			
			if not last_self_rev:
				early_delta = datetime.now() - last_revisions[-1].get('timestamp')
				last_delta: timedelta = datetime.now() - last_revisions[0].get('timestamp')
				# Edit jika salah satu benar.
				# - telah 2 minggu setelah 10 edit sebelumnya
				# - telah 2 hari setelah edit terakhir
				if early_delta.days < 14 and last_delta.days < 2:
					pywikibot.info(f'Dilompati: {title}: Halaman masih lumayan aktif untuk diganggu.')
					return True
			else:
				early_delta = datetime.now() - last_revisions[-1].get('timestamp')
				last_delta: timedelta = datetime.now() - last_other_rev.get('timestamp')
				# Edit jika salah satu benar.
				# - edit terakhir adalah sang bot
				# - telah 4 minggu setelah 10 edit sebelumnya
				# - telah 4 hari setelah edit terakhir
				if last_self_rev_index > 0 and early_delta.days < 28 and last_delta.days < 4:
					pywikibot.info(f'Dilompati: {title}: Halaman masih lumayan aktif untuk diganggu.')
					return True

		return super().skip_page(page)

	def treat_page(self) -> None:
		"""Load the given page, do some changes, and save it."""
		page = self.current_page

		title = page.title()
		init_text = page.text
		text = page.text
		# print(title)
		# print(text)
		is_negligble = True

		for match in linksInRef.finditer(text):
			if not get_citation_fixer(match['url']):
				continue
			print(f"<ref{match['name']}>{{{{Cite news|url={match['url']}}}}}</ref>")
			text = text.replace(match.group(), f"<ref{match['name']}>{{{{Cite news|url={match['url']}}}}}</ref>")
			is_negligble = False

		code = mwparserfromhell.parse(text)

		template: mwparserfromhell.wikicode.Template = 1

		pywikibot.info("┌──────────────────┬──────────────────────────────────────────────────────────────────┬──────────┐")

		for template in code.filter_templates():
			if not(cite_template_match(template) and get_citation_fixer(template.get('url', None))):
				continue 

			is_negligble_last = is_negligble
			(info, is_negligble_template) = get_info(template.get('url').value, template)
			# info = {}
			pywikibot.debug('====')
			pywikibot.debug(template)
			pywikibot.debug(info)

			if not info:
				continue

			# Cite web -> Cite news
			if cite_template_match(template) > 1:
				pywikibot.debug('Kutipan menggunakan Cite web, mengubah ke Cite news...')
				template.name = "Cite news"

			# (template, info) = alias_fix(template, info)
			alias_fix(template, info)

			# if not is_negligble_template:
			# 	is_negligble = False
			
			if '_authors' in info and '_editors' in info and '_name_rules' in info:
				set_names(template, info['_authors'], info['_editors'], info['_name_rules'])
				del info['_authors']
				del info['_editors']
				del info['_name_rules']

			minor_keys = ['work', 'language']

			for key in info.keys():
				info[key] = info[key].strip()
				# String biasa untu parameter tanpa alias
				if info[key] == "":
					if template.has(key):
						if str(template.get(key).value).strip() and key not in minor_keys:
							is_negligble = False
						template.remove(key)
				else:
					if template.has(key) and template.get(key).value.strip() != info[key].strip() and key not in minor_keys:
						is_negligble = False
					template.add(key, info[key])

			is_negligble = is_negligble and is_negligble_last
			
			# is_negligble = False

			pywikibot.debug(template)

		pywikibot.info("└──────────────────┴──────────────────────────────────────────────────────────────────┴──────────┘")

		if is_negligble:
			pywikibot.info("Perubahan teralu sedikit. Lompati.")
			text = init_text
		else:
			text = str(code)
		# if is_dupe_fix: 
		# 	text = str(code)

		# print(text)
		# print(text_doc)

		self.put_current(text, summary=self.opt.summary, minor=self.opt.minor)

def main(*argv: str) -> None:
	"""
	Process command line arguments and invoke bot.

	If args is an empty list, sys.argv is used.

	:param args: command line arguments
	"""
	args = dict(DEFAULT_ARGS)
	argv = pywikibot.handle_args(argv)

	gen_factory = pagegenerators.GeneratorFactory()
	argv = gen_factory.handle_args(argv)

	for arg in argv:
		arg, _, value = arg.partition(':')
		option = arg[1:]
		if option in ('summary', 'text'):
			if not value:
				pywikibot.input('Please enter a value for ' + arg)
			args[option] = value
		elif option == 'major':
			args['minor'] = False
		else:
			args[option] = True

	gen = gen_factory.getCombinedGenerator(preload=True)

	if not pywikibot.bot.suggest_help(missing_generator=not gen):
		bot = CitationFixerBot(generator=gen, **args)
		bot.run()  #

if __name__ == '__main__':
	main()
